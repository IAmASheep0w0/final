load standard_st_4.mat
% Num=0;
% Type=0;
% Standard_st=zeros([360,1]);
%% 获取平均图像（每个）

close all
type = 10;
num = 10;
dir = 'D:\yj\3D打印观察\2017-1-16\';
Array_I = false([1200, 1600, type, num]);
% figure_st = zeros([360, type]);
Figure_st = zeros([360, type]);
Array_st = zeros([360, type, num]);

X = linspace(1, 360, 360);

for i = 1 : type
    for j = 1 : num
        Array_I(:, :, i , j) = imread([dir, 'holes-3-5-', num2str(i), '-', num2str(j), '.tif']);
        Array_st(:, i, j) = Step_1_3(Array_I(:, :, i , j));          
    end
end
for i = 1 : type
    for j = 1 : num
        Array_st(:, i, j) = Array_st(:, i, j) / max(Array_st(:, i, j)) .* max(Standard_st);
        Figure_st(:, i) = Figure_st(:, i) + Array_st(:, i, j);
    end
     Figure_st(:, i) = Figure_st(:, i) ./ num;    
end



