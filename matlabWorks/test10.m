load Type.mat
load Num.mat
load standard_st_3.mat
% Num=0;
% Type=0;
% Standard_st=zeros([360,1]);
%% 获取平均图像（每个和所有）

close all
type = 10;
num = 10;
dir = 'D:\yj\3D打印观察\2017-1-16-2\';
Array_I = false([1200, 1600, type, num]);
% figure_st = zeros([360, type]);
Figure_st = zeros([360, type]);
Array_st = zeros([360, type, num]);
standard_st = zeros([360, 1]);
% Standard_st = zeros([360, 1]);

X = linspace(1, 360, 360);

for i = 1 : type
    for j = 1 : num
        Array_I(:, :, i , j) = imread([dir, 'holes-4-5-', num2str(i), '-', num2str(j), '.tif']);
        Array_st(:, i, j) = Step_1_3(Array_I(:, :, i , j));
        standard_st = standard_st + Array_st(:, i, j);
        
%         figure_st(:, i) = figure_st(:, i) + Array_st(:, i, j);
        
%         figure(1),
%         plot(X, Array_st(:, i, j));
%         hold on
%         figure(1 + i),
%         plot(X, Array_st(:, i, j));
%         hold on
        
    end
    
%     figure_st(:, i) = figure_st(:, i) ./ num;
    
%     standard_st(:) = standard_st(:) + figure_st(:, i);
    
%     figure(11 + i),
%     plot(X, Figure_st(:, i));
%     hold on
    
end

Standard_st(:) = (Standard_st(:) * Type * Num + standard_st) / (Type * Num + type * num);
Type = Type + type;
Num = Num + num;
for i = 1 : type
    for j = 1 : num
%         Array_st(:, i, j) = Array_st(:, i, j) / max(Array_st(:, i, j)) .* max(Standard_st);
        Figure_st(:, i) = Figure_st(:, i) + Array_st(:, i, j);
    end
     Figure_st(:, i) = Figure_st(:, i) ./ num;    
end


% figure(22),
% plot(X, Standard_st);

%% 获取特征值（频谱），并量化
% F = Step_2_3(Figure_st, Standard_st, type);
% 
% for i = 1 : type
%     F(33 : 360, i) = 0;
% end
% 
% F = round(F ./ 8);
% 
% for i = 1 : type
%     figure(23),
%     plot(X, F(:, i));
%     hold on
% end

%% 随机选择一个样本对阈值进行测试

% path = 'E:\LAB-Photos\2017-01-15\holes-1-5-2-4.tif';
% I = imread(path);
% test_st = Step_1_3(I);
% 
% F_ = Step_2_3(test_st, Standard_st, 1);
% F_(33 : 360) = 0;
% F_ = round(F_ ./ 1);

% figure(24),
% plot(X, F_);
%% 对所有的样本进行检验测试(test12)


