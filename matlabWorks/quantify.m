type = 10;
load standard_st4.mat;
load figure_st_1.mat;
%% 获取特征值（频谱），并量化
F1 = Step_2_3(Figure_st, Standard_st, type);
load figure_st_2.mat;
F2 = Step_2_3(Figure_st, Standard_st, type);
load figure_st_3.mat;
F3 = Step_2_3(Figure_st, Standard_st, type);
load figure_st_4.mat;
F4 = Step_2_3(Figure_st, Standard_st, type);
F=[F1,F2,F3,F4];
Fmax=zeros(48,1);
Fmin=zeros(48,1);
for i=1:48
    Fmax(i)=max(F(i,:));
    Fmin(i)=min(F(i,:));
end
