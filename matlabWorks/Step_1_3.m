function [St] = Step_1_3(I)
%% 需更改，求质心的函数应当另取。将整个轮廓面的点计入质心的计算中。

B = bwboundaries(I);
B = B(1);
for i = 1:length(B)
    b = B{i};
end

[Index_x, Index_y] = find(I == 1);
[Area, ~] = size(Index_x);

E = true([Area, 1]);

Index_x = Index_x - E;
Index_y = Index_y - E;

X_o = sum(Index_x) / Area;
Y_o = sum(Index_y) / Area;

% figure(25),
% plot(b(:, 1), b(:, 2));
% hold on

% [st, angle, x_o, y_o] = signature(b);
% plot(x_o, y_o, 'r*');
% hold on

[st, angle, x_o, y_o] = signature(b, X_o, Y_o);

%% 已更改-2017-01-14-16:23

% St = st;

% d_ = zeros([360, 1]);
% d = abs(diff(st));
% d_(1: 359) = d(:);
% d_(360) = abs(st(1) - st(360));

index = find(st == max(st));
step = index - 1;
St = zeros([360, 1]);

for i = 1:360
    if i >= index
        St(i - step) = st(i);
    else
        St(360 + (i - step)) = st(i);
    end
end

a = 1;
% f = dct(St);
% figure(15),
% plot(X, f);
% hold on
% F(:, 1) = f(xbegin : xend);
