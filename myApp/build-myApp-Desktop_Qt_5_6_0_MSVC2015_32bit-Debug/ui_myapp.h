/********************************************************************************
** Form generated from reading UI file 'myapp.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYAPP_H
#define UI_MYAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_myApp
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *myApp)
    {
        if (myApp->objectName().isEmpty())
            myApp->setObjectName(QStringLiteral("myApp"));
        myApp->resize(400, 300);
        menuBar = new QMenuBar(myApp);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        myApp->setMenuBar(menuBar);
        mainToolBar = new QToolBar(myApp);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        myApp->addToolBar(mainToolBar);
        centralWidget = new QWidget(myApp);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        myApp->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(myApp);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        myApp->setStatusBar(statusBar);

        retranslateUi(myApp);

        QMetaObject::connectSlotsByName(myApp);
    } // setupUi

    void retranslateUi(QMainWindow *myApp)
    {
        myApp->setWindowTitle(QApplication::translate("myApp", "myApp", 0));
    } // retranslateUi

};

namespace Ui {
    class myApp: public Ui_myApp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYAPP_H
