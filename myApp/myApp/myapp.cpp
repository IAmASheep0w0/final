#include "myapp.h"
#include "ui_myapp.h"

myApp::myApp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::myApp)
{
    ui->setupUi(this);
}

myApp::~myApp()
{
    delete ui;
}
