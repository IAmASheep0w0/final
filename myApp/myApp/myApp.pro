#-------------------------------------------------
#
# Project created by QtCreator 2017-02-22T11:09:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = myApp
TEMPLATE = app


SOURCES += main.cpp\
        myapp.cpp \
    mygamma.cpp \
    signature.cpp \
    reverse.cpp

HEADERS  += myapp.h \
    mygamma.h \
    main.h \
    signature.h \
    common.h \
    reverse.h

FORMS    += myapp.ui

INCLUDEPATH += D:/opencv/opencv/mybuild/install/include/opencv
INCLUDEPATH += D:/opencv/opencv/mybuild/install/include/opencv2
INCLUDEPATH += D:/opencv/opencv/mybuild/install/include

CONFIG   += debug_and_release
win32:
{
    LIBS     += -LD:/opencv/opencv/mybuild/install/x86/vc14/lib \
    -lopencv_aruco300d \
    -lopencv_bgsegm300d \
    -lopencv_bioinspired300d \
    -lopencv_calib3d300d \
    -lopencv_ccalib300d \
    -lopencv_core300d \
    -lopencv_dnn300d \
    -lopencv_face300d \
    -lopencv_features2d300d \
    -lopencv_flann300d \
    -lopencv_highgui300d \
    -lopencv_imgcodecs300d \
    -lopencv_imgproc300d \
    -lopencv_line_descriptor300d \
    -lopencv_ml300d \
    -lopencv_objdetect300d \
    -lopencv_photo300d \
    -lopencv_reg300d \
    -lopencv_rgbd300d \
    -lopencv_saliency300d \
    -lopencv_shape300d \
    -lopencv_stereo300d \
    -lopencv_stitching300d \
    -lopencv_superres300d \
    -lopencv_surface_matching300d \
    -lopencv_video300d \
    -lopencv_videoio300d \
    -lopencv_videostab300d \
    -lopencv_xfeatures2d300d \
    -lopencv_xobjdetect300d \
    -lopencv_xphoto300d \
    -lopencv_bgsegm300 \
    -lopencv_bioinspired300 \
    -lopencv_calib3d300 \
    -lopencv_ccalib300 \
    -lopencv_core300 \
    -lopencv_dnn300 \
    -lopencv_face300 \
    -lopencv_features2d300 \
    -lopencv_flann300 \
    -lopencv_highgui300 \
    -lopencv_imgcodecs300 \
    -lopencv_imgproc300 \
    -lopencv_line_descriptor300 \
    -lopencv_ml300 \
    -lopencv_objdetect300 \
    -lopencv_photo300 \
    -lopencv_reg300 \
    -lopencv_rgbd300 \
    -lopencv_saliency300 \
    -lopencv_shape300 \
    -lopencv_stereo300 \
    -lopencv_stitching300 \
    -lopencv_superres300 \
    -lopencv_surface_matching300 \
    -lopencv_video300 \
    -lopencv_videoio300 \
    -lopencv_videostab300 \
    -lopencv_xfeatures2d300 \
    -lopencv_xobjdetect300 \
    -lopencv_xphoto300
}
