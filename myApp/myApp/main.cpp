#include "main.h"
using namespace cv;
//using namespace std;

int main(int argc, char *argv[])
{
    /*QApplication a(argc, argv);
    myApp w;
    w.show();

    return a.exec();*/
    Mat src = imread("D:/demo.bmp");
    Mat dst;

//    namedWindow("RGB", CV_WINDOW_NORMAL);
//    namedWindow("GRAY", CV_WINDOW_NORMAL);

//    imshow("RGB", src);

    cvtColor(src, dst, CV_BGR2GRAY);
//    imshow("GRAY", dst);

    float gamma = 2.0;
    myGamma(dst, gamma);

    namedWindow("GAMMA", CV_WINDOW_NORMAL);
    imshow("GAMMA", dst);

    threshold(dst, dst, 0, 255, THRESH_BINARY + THRESH_OTSU);

    namedWindow("BINARIZATION", CV_WINDOW_NORMAL);
    imshow("BINARIZATION", dst);

//    CvMemStorage *contours;
//    CvSeq **contour;
//    cvFindContours(dst, contours, contour);

    waitKey(0);
    src.release();
    dst.release();
    destroyAllWindows();

    return 0;
}
