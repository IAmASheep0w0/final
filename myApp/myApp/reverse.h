#ifndef REVERSE_H
#define REVERSE_H

#endif // REVERSE_H

#include "common.h"
using namespace cv;

void reverse (Mat &src, Mat &dst);
